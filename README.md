# 开源龙芯1c资料
[龙芯官方的ftp](http://www.loongnix.org/dev/ftp/embed/ls1c/)<br/>
[刘世伟的git](https://github.com/lshw)<br/>
[孙冬梅老师的git](https://github.com/sundm75)<br/>
孙冬梅老师的网盘 链接：https://pan.baidu.com/s/1eS3lffc 密码：0v6f，如果这个链接失效请去孙老师git中查看最新的网盘地址和密码<br/>
[勤为本的git](https://gitee.com/caogos)<br/>
[勤为本的网盘](http://pan.baidu.com/s/1dDB5ZWH)<br/>
[chinesebear](https://github.com/chinesebear)<br/>
[开龙论坛](http://www.openloongson.org)<br/>
[百度龙芯吧](http://tieba.baidu.com/f?kw=%C1%FA%D0%BE&amp;fr=ala0)<br/>
<br/>
<br/>
# Linux和pmon
主要参考刘世伟的git<br/>
<br/>
# 龙芯1x库
目前重点封装龙芯1c库，以后可能会有支持1b、1d等。龙芯1x库类似于stm32库，有了它，裸机编程和实时系统就变得so easy！<br/>
[龙芯1c库](https://gitee.com/caogos/OpenLoongsonLib1c)<br/>
<br/>
# RT-Thread
[RT-Thread的官方git](https://github.com/RT-Thread/rt-thread)<br/>
RT-Thread支持龙芯1c和龙芯1B，目前对龙芯1c支持是比较好的，各种功能基本移植上去了，比如gpio，pwm，定时器，i2c，中断，网卡，RGB屏（lcd）等，还在逐步完善中<br/>
<br/>
# 裸机编程
裸机编程主要是利用“龙芯1c库”实现，参考前面的“龙芯1c库”的资料<br/>



