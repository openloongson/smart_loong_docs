#include <QtGui>

int main(int argc, char *argv[])
{
        QApplication app(argc, argv);

	// 设置编码
//     	QTextCodec *codec = QTextCodec::codecForName("System");
//     	QTextCodec *codec = QTextCodec::codecForName("GB2312");
     	QTextCodec *codec = QTextCodec::codecForName("UTF-8");
//     	QTextCodec *codec = QTextCodec::codecForName("GBK");
        QTextCodec::setCodecForLocale(codec);
        QTextCodec::setCodecForCStrings(codec);
        QTextCodec::setCodecForTr(codec);

	// 待显示的内容
        QString str(QObject::tr("您好"));
        QLabel hello(str);

	// 设置字体
        QFont font = hello.font();
        font.setPixelSize(36);
//	font.setFamily("unifont");
	font.setFamily("WenQuanYi Micro Hei Mono");
	font.setBold(false);
        hello.setFont(font);

	// 设置窗体大小
        hello.resize(320, 240);
        hello.show();

        return app.exec();
}

