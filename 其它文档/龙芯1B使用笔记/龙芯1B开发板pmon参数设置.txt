环境介绍
linux开发主机ip地址为“192.168.1.5”，nfs根文件夹的存放路径为/nfsramdisk/LS1Brootfs-demo
龙芯1B开发板ip地址为“192.168.1.6”

设置ip地址
set ifconfig syn0:192.168.1.6

tftp下载内核，nfs挂载根文件系统
load tftp://192.168.1.5/vmlinuz
g root=/dev/nfs rw nfsroot=192.168.1.5:/nfsramdisk/LS1Brootfs-demo noinitrd init=/linuxrc console=ttyS2,115200 ip=192.168.1.6:::::eth0:off

set al tftp://192.168.1.5/vmlinuz
set append 'root=/dev/nfs rw nfsroot=192.168.1.5:/nfsramdisk/LS1Brootfs-demo noinitrd init=/linuxrc console=ttyS2,115200 ip=192.168.1.6:::::eth0:off'



开发板默认pmon参数——从flash加载内核和根文件系统
PMON> env
    append = "root=/dev/mtdblock1 console=ttyS2,115200 noinitrd init=/linuxrc rw rootfstype=yaffs2"
        al = /dev/mtd0 
   ethaddr = e6:be:06:f9:08:d6
  pll_reg0 = 0x00000012
  pll_reg1 = 0x92292a00
      xres = 480       
      yres = 272       
     depth = 16        
   memsize = 64        
highmemsize = 0         
  cpuclock = 247500000 
  busclock = 123750000 
   systype = FCR       
    brkcmd = "l -r @cpc 1"
  datasize = -b          [-b -h -w]
    dlecho = off         [off on lfeed]
   dlproto = none        [none XonXoff EtxAck]
     bootp = no          [no sec pri save]
  hostport = tty0      
   inalpha = hex         [hex symbol]
    inbase = 16          [auto 8 10 16]
    moresz = 10        
    prompt = "PMON> "
  regstyle = sw          [hw sw]
    rptcmd = trace       [off on trace]
   trabort = ^K        
      ulcr = cr          [cr lf crlf]
     uleof = %         
   showsym = yes         [no yes]
     fpfmt = both        [both double single none]
     fpdis = yes         [no yes]
        TZ = UTC8      
  ifconfig = syn0:192.168.1.2
update_usb = no          [no yes]
syn0.ipaddr = 192.168.1.2
PMON>  










